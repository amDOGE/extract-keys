const fileInput = document.querySelector('input[type="file"]');
const preview = document.querySelector('img.preview');
const eventLog = document.querySelector('.event-log-contents');
const reader = new FileReader();

function handleEvent(event) {
    if (event.srcElement && !event.srcElement.result) return;
    eventLog.textContent = '';
    document.querySelector('.qr-container').innerHTML = '';
    const pass = document.getElementById("pass").value;
    const dec = CryptoJS.AES.decrypt(event.srcElement.result.toString().replace(/\n/g,""), pass).toString(CryptoJS.enc.Utf8);
    eventLog.textContent = "";
    let count = 0;
    for (const line of dec.split('\n')) {
        const [mightBeWIF] = line.split(' ');
        if (!line.startsWith("#") && mightBeWIF.length > 0) {
          eventLog.textContent += mightBeWIF;
          eventLog.textContent += "\n";
          const el = document.createElement('div');
          const id = `qrcode-${++count}`
          el.id = id;
          document.querySelector('.qr-container').appendChild(el);
          document.querySelector('.qr-container').appendChild(document.createTextNode(mightBeWIF));
          new QRCode(document.getElementById(id), mightBeWIF);
        }
    }
}

function addListeners(reader) {
    reader.addEventListener('loadstart', handleEvent);
    reader.addEventListener('load', handleEvent);
    reader.addEventListener('loadend', handleEvent);
    reader.addEventListener('progress', handleEvent);
    reader.addEventListener('error', handleEvent);
    reader.addEventListener('abort', handleEvent);
}

function handleSelected(e) {
    eventLog.textContent = '';
    document.querySelector('.qr-container').innerHTML = '';
    const selectedFile = fileInput.files[0];
    if (selectedFile) {
        addListeners(reader);
        reader.readAsText(selectedFile);
    }
}

fileInput.addEventListener('change', handleSelected);
